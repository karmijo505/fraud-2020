<html>
<head>
    <style>
        .headcenter {
            text-align: center;
            color: red;
        }

        .w-100 {
            width: 100%;
        }

        .w-75 {
            width: 75%;
        }

        .w-65 {
            width: 65%;
        }

        .w-60 {
            width: 60%;
        }

        .w-55 {
            width: 55%;
        }

        .w-50 {
            width: 50%;
        }

        .w-45 {
            width: 45%;
        }

        .w-40 {
            width: 40%;
        }

        .w-35 {
            width: 35%;
        }

        .w-30 {
            width: 30%;
        }

        .w-20 {
            width: 20%;
        }

        .f-left {
            float: left;
        }

        .f-right {
            float: right;
        }

        .t-left {
            text-align: left;
        }

        .t-right {
            text-align: right;
        }

        .t-center {
            text-align: center
        }

        .t-size10 {
            font-size: 1.10em;
        }

        .t-size12 {
            font-size: 12px;
        }
        .t-size16 {
            font-size: 16px;
        }

        .center {
            margin: 0 auto;
        }

        .bold {
            font-weight: bold;
        }

        .red-text {
                color: red;
         }

        .company-info {
            padding: 10px;
        }

        .company-info-2 {
            padding: 10px 0;
        }

        .notification-info {
            padding: 35px 0;
        }

        .office-use-info {
            padding: 25px;
        }

        .foot {
            position: fixed;
            text-align: center;
            width: 100%;
            padding: 30px;
            bottom: 0;
        }

        .mt-30 {
            margin-top: 30px;
        }

        .text-center {
            text-align: center;
        }

        table th {
            border: 1px solid #444;
        }

        fieldset {
            border: 1px solid #444;
            padding: 15px;
            width: 50%;
        }

        @page {
            margin-top: 25px;
            margin-bottom: 25px;
            margin-left: 25px;
            margin-right: 25px;
        }
    </style>
</head>


@extends('layouts.header')

@section('content')

    <div>
        <div class="company-info f-left w-45 t-size12">
            <div>NAIC #: {{ $entityData['naic_cocode'] }}</div>
            <div>{{ $entityData['company_name'] }}</div>
            <div>{{ $entityData['mlg_address1'] }}</div>
            <span>{{ $entityData['mailing_city'] }}</span>
            <span>{{ $entityData['mailing_state'] }}, </span>
            <span>{{ $entityData['mailing_zip'] }}</span>
        </div>

        <div class="f-right w-45 t-left">
            <div class="company-info f-left w-45 t-left t-size12">
                <div>Date Invoice Generated</div>
                <div>Invoice Number</div>
                <div>Assessment Year</div>
                <div>Total Amount Due By</div>
            </div>
            <div class="company-info f-right w-30 t-left t-size12">
                <div>{{ $noticeDate }}</div>
                <div>{{ $invoiceNbr }}</div>
                <div>{{ $taxableYear }}</div>
                <div>{{ $dueDate }}</div>
            </div>
        </div>
    </div>
    <div>
    <div class="t-size12 mt-30 red-text">
        <span style="background-color: #ffff00">
            <span class="bold">Important:  Checks are no longer accepted.</span>
            <div><span style="background-color: #ffff00">Payments must be submitted via Wire or ACH Credit.</span></div>
        </span>
        </div>
    </div>



    <div>
        <h3 class="t-center">ANNUAL FRAUD ASSESSMENT</h3>

        <table width="100%">
            <tr>
                <th>Direct Premium Written</th>
                <th>Assessment</th>
            </tr>
            @foreach ($entityData['lobs'] as $lob)
                <tr>
                    <td class="t-left">${{  number_format($lob['premium'], 2) }}</td>
                    <td class="t-center">${{ $lob['assessment'] != 0 ? number_format($lob['assessment'], 2)  : '' }}</td>
                </tr>
            @endforeach
            <tr>
                <td class="t-right">Total amount to be Remitted:</td>
                <td class="t-center bold">${{ number_format($totalAmt, 2, '.', ',') }}</td>
            </tr>
        </table>
    </div>

    <div class="headcenter t-size16 mt-30">
        <span style="background-color: #ffff00">
            <span class="bold">Attention</span>: Please include in your addenda via Wire or ACH Credit:
        </span>
    </div>

    <!-- ADDENDA INCLUDE -->

    <div class="text-center t-size16 bold">
        {{ $addendaInclude }}
    </div>

    <div class="notification-info w-100 t-left t-size12">
        Pursuant to Section 59A-16C-14B NMSA 1978:  The fee shall be due annually pursuant to rules promulgated by the Superintendent.  The failure of an insurer to pay this fee when due shall subject the insurer to a penalty of one thousand dollars ($1,000) per month or part thereof in which the fee remains unpaid.
    </div>

    <div>

        <div class="t-center t-size12" >Distribution - OSI Use Only</div>

        <table width="100%">
            <tr>
                <th>Payment Type</th>
                <th>Account</th>
                <th>Assessment</th>
            </tr>
            @foreach ($entityData['lobs'] as $lob)
                <tr>
                    <td class="t-left">{{  $lob['line_of_business'] }}</td>
                    <td class="t-right">89</td>
                    <td class="t-center">${{ $lob['assessment'] != 0 ? number_format($lob['assessment'], 2)  : '' }}</td>
                </tr>
            @endforeach
        </table>
    </div>

    <div class="foot" style="margin-top:130px">
        <img src="/images/osi-transparent.png" width="125px" height="80px">
    </div>
    <div class="headcenter t-size12" style="margin-bottom: -30px">
        <div>Main Office: 1120 Paseo de Peralta, Room 428, Santa Fe, NM 87501</div>
        <div>Satellite Office:95200 Uptown Blvd NE, Suite 100, Albuquerque, NM 87110</div>
        <div>Main Phone: (505) 827-4601 | Satellite Phone: (505) 322-2186 | Toll Free: (855) 4-ASK-OSI</div>
        <div>www.osi.state.nm.us</div>
    </div>

    <p style="page-break-before: always">
    <br>
<div>
    <h2 class="headcenter">STATE OF NEW MEXICO<br>
        OFFICE OF SUPERINTENDENT OF INSURANCE</h2>
    <div style="margin-bottom:15px">
        <div class="f-left w-25">
            <div class="headcenter">SUPERINTENDENT</div>
            <div class="headcenter">OF INSURANCE</div>
            <div class="headcenter">Russell Toal</div>
        </div>
        <div class="f-left w-45 headcenter"><img src="/images/osi-transparent.png" width="85px" height="60px" style="margin-bottom:15px">
            {{--        <div class="headcenterBold">SUPERINTENDENT OF INSURANCE</div>
                    <div class="headcenter">Russell Toal</div>--}}
        </div>
        <div class="f-right w-25 headcenter">
            <div class="headcenter">DEPUTY SUPERINTENDENT</div>
            <div class="headcenter">Robert E. Doucette, Jr.</div>
        </div>
    </div>
    </div>

    <div style="padding:10px">
        <div class="company-info f-left w-45 t-size12">
            <div>{{ $noticeDate }}</div>
                <br>
            <div>NAIC #: {{ $entityData['naic_cocode'] }}</div>
            <div>{{ $entityData['mlg_address1'] }}</div>
            <span>{{ $entityData['mailing_city'] }}</span>
            <span>{{ $entityData['mailing_state'] }}, </span>
            <span>{{ $entityData['mailing_zip'] }}</span>
        </div>

        <div class="company-info-2 w-100 t-left t-size10">
            <p>The Office of Superintendent of Insurance (OSI) has switched all Annual Fraud Assessment payments to <span style="color:red">ONLY</span> be processed electronically through an ACH Credit or Wire.   Payments by check are Not Accepted and will be returned by Certified Return Receipt.</p>

            <p><span style="color:black"><strong><u>ALL</u></strong></span> companies are required to make the Annual Fraud Assessment payment by ACH Credit or Wire. The information to set up the ACH Credit or Wire is below.  When paying the Annual Fraud Assessment, a separate ACH Credit or Wire must be submitted and should <span style="color:red">NOT</span> be combined with <span style="color:red">Certificate of Authority, Annual Statement Payments nor any other payment.</span></p>

            <p>No later than July 17, 2020 the Annual Fraud Assessment Invoices will be available for companies to download from the OSI Website (www.osi.state.nm.us) under Departments/Criminal Division.  <span style="color:red">YOU MUST USE</span><strong> Chrome or Firefox</strong> for downloading the Annual Fraud Assessment Invoices.  The website will have updates as needed.</p>

            <p>The information for setting up the ACH Credit and Addenda is as follows:</p>
            <div class="t-size12">
            <div><span style="color:red"><strong>Here is the information for the ACH Credit/Wire:</strong></span></div>
            <div style="color:blue">
                <div><strong>Receiving Bank:</strong> Wells Fargo Bank, N.A.</div>
                <div><strong>Bank Address:</strong> 420 Montgomery, San Francisco, CA 94104</div>
                <div><strong>Account Name:</strong> State of NM Office of Superintendent of Insurance</div>
                <div><strong>Bank Account Number:</strong> 4124307299</div>
                <div><strong>Bank Account Routing Number:</strong> 121000248</div>
        </div>
        </div>
        </div>

        <div class="foot" style="margin-top:-10px">
            <img src="/images/osi-transparent.png" width="125px" height="80px">
        </div>
        <div class="headcenter t-size12" style="margin-bottom: -20px">
            <div>Main Office: 1120 Paseo de Peralta, Room 428, Santa Fe, NM 87501</div>
            <div>Satellite Office: 6200 Uptown Blvd NE, Suite 100, Albuquerque, NM 87110</div>
            <div>Main Phone: (505) 827-4601 | Satellite Phone: (505) 322-2186 | Toll Free: (855) 4-ASK-OSI</div>
            <div>www.osi.state.nm.us</div>
        </div>

        <p style="page-break-before: always">
        <div>
            <img src="/images/addenda.jpg" width="100%" style="margin-bottom: 80px">
        </div>
    </div>
@endsection
</html>
